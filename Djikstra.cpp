#include<bits/stdc++.h>
using namespace std;

typedef pair<int, int> ii;//untuk menghubungkan dua nilai
typedef vector<int> vi;//dynammic array dengan ability untuk resize sendiri ketika akan ada yang di deleted atau di insert
typedef vector<ii> vii;//kita untuk menggantikan array,, vector juga menyimpan elemen-elemen secara bersebelahan dan elemen tesebut dapat diakses sesuai subscript/index
typedef long long ll;
typedef unsigned long long ull;
#define INF 1000000000

//ingat rii!! Ukuran vector dapat berubah secara dinamis dan dengan pengalokasian memori yang ditangani secara otomatis oleh vector itu sendiri.

vector<vii> adjList(27, vii()); // pair tadi masukkan lagi ke vector (-_-)
string city[] = {
	"IT Del",
	"GKPI TANDING Resort Balige",
	"GBI Gasaribu",
	"SPBU 14.223.304 Laguboti",
	"Kantor Pelayanan Perbendaharaan Negara",
	"Batikta",
	"Museum Balige",
	"Simpang Hutabulu Mejan - Jln. Raya ...",
	"Simpang Hutabulu Mejan - Jln. Sama Muda",
	"Kedai Bakmi Boni",
	"Warung Makan Muslim Bu Nuul",
	"RM. Muslim Berkah",
	"Simpang GM. Siahaan",
	"RM. Mas Yono",
	"Alfamidi",
	"RM. Karina Jawa",
	"RM. Panca",
	"RM. Sabas",
	"RM. Muslim Marroan",
	"Bahagia Cafe & Resto",
	"Monumen Pahlawan Revolusi",
	"Mess Pemprovsu",
	"Kantor Dinas Perhubungan",
	"Komni Madite",
	"Smile Coffee",
	"RM. Gumarang Balige",
	"Quality Fried Chicken"
};

void generateGraph(){
	adjList[18].push_back({17, 180});//menambhakan elemen kedalam vector
	adjList[18].push_back({16, 200});//push_back : menambah elemen di akhir artinya kan,,,,
	adjList[18].push_back({19, 55});// node awal ke route destination
	
	adjList[16].push_back({18, 200});
	adjList[16].push_back({14, 150});
	
	adjList[17].push_back({18, 180});
	adjList[17].push_back({14, 160});
	
	adjList[14].push_back({17, 160});
	adjList[14].push_back({16, 150});
	adjList[14].push_back({15, 150});
	adjList[14].push_back({13, 150});
	
	adjList[15].push_back({19, 210});
	adjList[15].push_back({11, 140});
	adjList[15].push_back({14, 150});
	
	adjList[13].push_back({14, 150});
	adjList[13].push_back({12, 140});
	
	adjList[19].push_back({18, 55});
	adjList[19].push_back({15, 210});
	adjList[19].push_back({20, 82});
	adjList[19].push_back({26, 170});
	
	adjList[26].push_back({19, 170});
	adjList[26].push_back({25, 89});
	
	adjList[25].push_back({26, 89});
	adjList[25].push_back({24, 550});
	adjList[25].push_back({22, 270});
	
	adjList[24].push_back({25, 550});
	adjList[24].push_back({23, 210});
	adjList[24].push_back({22, 700});
	
	adjList[20].push_back({21, 200});
	adjList[20].push_back({19, 82});
	
	adjList[11].push_back({15, 140});
	adjList[11].push_back({10, 550});
	adjList[11].push_back({12, 290});

	adjList[12].push_back({11, 290});
	adjList[12].push_back({13, 140});
	adjList[12].push_back({8, 3000});
	
	adjList[8].push_back({12, 3000});
	adjList[8].push_back({9, 700});
	adjList[8].push_back({7, 3000});
	
	adjList[10].push_back({11, 550});
	adjList[10].push_back({9, 350});
	
	adjList[21].push_back({20, 200});
	adjList[21].push_back({22, 120});
	
	adjList[22].push_back({21, 120});
	adjList[22].push_back({25, 270});
	adjList[22].push_back({24, 700});
	adjList[22].push_back({23, 700});
	
	adjList[23].push_back({22, 700});
	adjList[23].push_back({24, 210});
	
	adjList[9].push_back({10, 350});
	adjList[9].push_back({8, 700});
	adjList[9].push_back({5, 750});
	
	adjList[7].push_back({8, 160});
	adjList[7].push_back({6, 170});

	adjList[6].push_back({7, 170});
	adjList[6].push_back({5, 650});
	
	adjList[5].push_back({6, 650});
	adjList[5].push_back({9, 750});
	adjList[5].push_back({4, 1700});
	
	adjList[4].push_back({5, 1700});
	adjList[4].push_back({3, 2500});
	
	adjList[3].push_back({4, 2500});
	adjList[3].push_back({2, 2095});
	
	adjList[2].push_back({3, 2095});
	adjList[2].push_back({0, 2500});
	adjList[2].push_back({1, 3300});
	
	adjList[1].push_back({2, 3300});
	adjList[1].push_back({0, 1600});
	
	adjList[0].push_back({1, 1600});
	adjList[0].push_back({2, 2500});
}

int searchStartIndex(string &startCity){
	for(int i = 0; i < sizeof(city); i++){
		if(startCity == city[i]){
			return i;
		}
	}
	
	return -1;// isonma dimpamasuk node berangkat
}

int searchEndIndex(string &endCity){
	for(int i = 0; i < sizeof(city); i++){
		if(endCity == city[i]){
			return i;
		}
	}
	
	return 100;
}// dison nide tujuan

int main(){
	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	//Ini mengaktifkan atau menonaktifkan sinkronisasi 
	//semua stream standar C ++ dengan stream C standar yang sesuai 
	//jika dipanggil sebelum program melakukan operasi input atau output pertamanya
	vector<vi> routes(27, vi());
	
	cout << "Constructing graph..." << endl;
	generateGraph();
	cout << "Finished constructing graph" << endl;

	int iStart;
	int iEnd;
	string start;
	string end;
	
	puts("Input start city: ");
	getline(cin, start);// inilah yang diaktifkan tadi
	puts("Input destination city: ");
	getline(cin, end);
	iStart = searchStartIndex(start);
	iEnd = searchEndIndex(end);
	
	vi distance(27, INF);
	distance[iStart] = 0;
	
	priority_queue<ii, vii, greater<ii> > priorityQueue;   // node yang dilalui tadi dimasukkan lagi ke queue/antrian
	priorityQueue.push({0, iStart});


// code prosesnya
	while(!priorityQueue.empty()){
		ii temporary = priorityQueue.top();
		priorityQueue.pop();
		
		int city = temporary.second;// pair nya
		int weight = temporary.first;//pair kedua
		
		if(weight > distance[city]) continue;
		for(int i = 0; i < adjList[city].size(); i++){
			ii nextCityPair = adjList[city][i];
			int nextCity = nextCityPair.first;
			int nextCityWeight = nextCityPair.second;// pair juga
			
			if(distance[city] + nextCityWeight < distance[nextCity]){
				distance[nextCity] = distance[city] + nextCityWeight;//perhitungan/proses pengecekan mana yang akan dibuat kedalam antrian/ queue
				priorityQueue.push({distance[nextCity], nextCity});
				routes[nextCity].clear();//menghapus semua isi vector
				
				for(int j = 0; j < routes[city].size(); j++){
					routes[nextCity].push_back(routes[city][j]);
				}
				
				routes[nextCity].push_back(city);
			}
		}
	}
	
	cout << "Recommended route from " << city[iStart] << " to " << city[iEnd] << " are:\n";
	for(int i = 0; i < routes[iEnd].size(); i++){//size: menampilkan jumlah elemen pada vector
		cout << city[routes[iEnd][i]] << " -> ";
	}

	cout << city[iEnd] << endl;//end: mengembalikan iterator ke akhir vector
	cout << "Total distance: " << distance[iEnd] << " m\n";

	return 0;
}
